//
//  SecondVC.m
//  Test
//
//  Created by Craterzone on 14/02/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import "SecondVC.h"

@interface SecondVC ()
@property (strong,nonatomic) NSString *buttonName;
@property (strong,nonatomic) UIButton *myButton;
@end

@implementation SecondVC
@synthesize delegate,buttonName,myButton,indexNumber;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)setButton:(NSString*)buttonTitle
{
    buttonName=buttonTitle;
    myButton=[[UIButton alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
    myButton.backgroundColor=[UIColor purpleColor];
    [myButton setTitle:buttonTitle forState:UIControlStateNormal];
    [myButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:myButton];

}


-(IBAction)buttonClick:(id)sender
{
    if ([buttonName caseInsensitiveCompare:@"OFF"] == NSOrderedSame) {
        [[self delegate] callBack:indexNumber:@"ON"];
        [myButton setTitle:@"ON" forState:UIControlStateNormal];
    } else {
        [[self delegate] callBack:indexNumber:@"OFF"];
    [myButton setTitle:@"OFF" forState:UIControlStateNormal];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
