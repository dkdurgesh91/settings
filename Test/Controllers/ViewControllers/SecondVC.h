//
//  SecondVC.h
//  Test
//
//  Created by Craterzone on 14/02/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SampleDelegate <NSObject>
-(void)callBack:(NSIndexPath*)indexNumber :(NSString*)value;

@end

@interface SecondVC : UIViewController
-(void)setButton:(NSString*)buttonTitle;
@property (strong,nonatomic) id <SampleDelegate> delegate;
@property NSIndexPath *indexNumber;
@end
