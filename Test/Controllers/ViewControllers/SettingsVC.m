//
//  SettingsVC.m
//  Test
//
//  Created by Craterzone on 13/02/16.
//  Copyright © 2016 Craterzone. All rights reserved.
//

#import "SettingsVC.h"
#import "SecondVC.h"
@interface SettingsVC ()<UITableViewDelegate,UITableViewDataSource,SampleDelegate>
@property UITableView *table;
@property NSArray*name;
@property NSMutableArray *status;
@property NSIndexPath* index;

@end

@implementation SettingsVC
@synthesize name,status;
@synthesize  table,index;
- (void)viewDidLoad {
    [super viewDidLoad];
        name=@[
           @[@"General",@"Privacy",@"Notification",@"Lock",@"Setting"],
           @[@"Sim",@"Wifi",@"Bluetooth"],
           @[@"Camera",@"Photo",@"Sound",],
           @[@"Email",@"Icloud",@"Maps",@"News",@"Safari",]
           ];
    status=[[NSMutableArray alloc]initWithCapacity:4];
            [status insertObject:[NSMutableArray arrayWithObjects:@"OFF",@"OFF",@"OFF",@"OFF",@"OFF", nil] atIndex:0];
            [status insertObject:[NSMutableArray arrayWithObjects:@"OFF",@"OFF",@"OFF", nil] atIndex:1];
            [status insertObject:[NSMutableArray arrayWithObjects:@"OFF",@"OFF",@"OFF", nil] atIndex:2];
            [status insertObject:[NSMutableArray arrayWithObjects:@"OFF",@"OFF",@"OFF",@"OFF",@"OFF", nil] atIndex:3];
            
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated
{
    self.title=@"Setting";
    
    self.navigationController.navigationBar.translucent=NO;
    table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    table.delegate=self;
    table.dataSource=self;
    [table registerClass:[UITableViewCell class] forCellReuseIdentifier:@"TableView"];
    [self.view addSubview:table];
    [table reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [name[section] count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableView"];
    if(cell==nil)
    {
        cell = [UITableViewCell alloc];
    }
    cell= [cell initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"TableView"];
    cell.backgroundColor=[UIColor groupTableViewBackgroundColor];

    if(index.row==indexPath.row && index.section==indexPath.section)
    {
        [cell.layer setCornerRadius:5.5f];
        [cell.layer setBorderWidth:1.0f];
        cell.textLabel.text=[name[indexPath.section] objectAtIndex:indexPath.row];
        cell.detailTextLabel.text=[status[indexPath.section] objectAtIndex:indexPath.row];
        cell.imageView.image=[UIImage imageNamed:[name[indexPath.section] objectAtIndex:indexPath.row]];
        cell.textLabel.font=[UIFont fontWithName:@"Verdana" size:20];
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        cell.editingAccessoryType=UITableViewCellAccessoryDetailDisclosureButton;
    }else{
    [cell.layer setCornerRadius:5.5f];
    [cell.layer setBorderWidth:1.0f];
    cell.textLabel.text=[name[indexPath.section] objectAtIndex:indexPath.row];
    cell.detailTextLabel.text=[status[indexPath.section] objectAtIndex:indexPath.row];
    cell.imageView.image=[UIImage imageNamed:[name[indexPath.section] objectAtIndex:indexPath.row]];
    cell.textLabel.font=[UIFont fontWithName:@"Verdana" size:20];
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0f;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView=[[UIView alloc]init];
    headerView.backgroundColor=[UIColor lightGrayColor];
    [headerView.layer setBorderWidth:2.0f];
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(100, 1, 200, 20)];
    [label setFont:[UIFont fontWithName:@"Verdana" size:16.0]];
    if (section==0) {
        label.text=@"General Settings";
    }
    if(section==1)
    {
    label.text=@"Connectivity";
    }
    if(section==2)
    {
    label.text=@"Media";
    }
    if(section==3)
    {
    label.text=@"Accounts";
    }
    [headerView addSubview:label];
    return  headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  57;
}

-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    SecondVC *second=[[SecondVC alloc]initWithNibName:@"SecondVC" bundle:nil];
    second.indexNumber=indexPath;
    [second setButton:selectedCell.detailTextLabel.text];
    [second setDelegate:self];
    [self.navigationController pushViewController:second animated:YES];
    
    return indexPath;
}

-(void)callBack:(NSIndexPath*)indexNumber :(NSString*)value
{
    [status[indexNumber.section] replaceObjectAtIndex:indexNumber.row withObject:value];
    self.index=indexNumber;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
